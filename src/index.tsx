import React from 'react';
import ReactDOM from 'react-dom/client';
import './normalize.css';
import './fonts.css';
import './index.css';
import App from './Pages/App/App';
import {data} from "./selectsData"

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <App usersData={data}/>
  </React.StrictMode>
);

