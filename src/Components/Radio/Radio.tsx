import './Radio.css';

type Props = {
    labelValue: string,
    value: string,
    name: string,
    id: string,
    classInput: string,
    classLabel: string,
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    checked?: boolean,
    current: string,
}

export const Radio: React.FC<Props> = ({ name, id, classInput = "", classLabel, onChange, labelValue, value, current}) => {
    return (
        <>
            <input type="radio" name={name} id={id} className={classInput + " input_hidden"} value={value.toLowerCase()} onChange={(e)=>onChange(e)} 
            checked={current===value} />
            <label htmlFor={id} className={classLabel}>{labelValue}</label>
        </>
    )
}

