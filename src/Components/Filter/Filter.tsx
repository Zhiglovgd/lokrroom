import { useState } from 'react';
import './Filter.css';
import  {ReactComponent as FindIcon} from  '../../assets/icons/find.svg'
import { Submenu } from '../Submenu/Submenu';
import { Radio } from '../Radio/Radio';
import Checkbox from '../Checkbox/Checbox';
import { setting, country, filterQuery } from '../../types';
import DoubleRangeSlider from '../DoubleRangeSlider/DoubleRangeSlider.jsx';



type Props = {
    sports: Array<setting>,
    genders: Array<setting>,
    countries: Array<country>,
    inputValue: string,
    inputChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    settingsChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    curSports: Array<string>,
    curCountries: Array<string>,
    curSex: string,
    curFilter: string
}

const Filter: React.FC<Props> = ({ sports, genders, countries, curSports, curCountries, curSex, curFilter, inputValue, inputChange, settingsChange }) => {
    return (
        <>
            <div className="filter-menu">
                <div className="filter-search">
                    <div className="filter-search__btn">
                        <div className="search-icon">
                            <FindIcon/>
                        </div>
                    </div>
                    <input className="filter-search__input" type="text" name="search"
                        id="search" placeholder="search" value={inputValue} onChange={(e) => inputChange(e)} />
                </div>
                <p className="filter-menu__name">filter</p>
                <div className="radio-container">
                    {<Radio name='filter' id='all' classInput='radio-container__input' classLabel='radio-container__label'
                        onChange={settingsChange} labelValue='all' value='all' current={curFilter} />}
                    {<Radio name='filter' id='draft' classInput='radio-container__input' classLabel='radio-container__label'
                        onChange={settingsChange} labelValue='draft' value='draft' current={curFilter} />}
                </div>
                <ul className="settings">
                    <li className="settings__item">
                        <Submenu name="gender" additionalClass='settings__submenu_gender'>
                            {genders.map((el) =>
                                <li className="submenu__item submenu__item_gender" key={el.value}>
                                    <Radio name='sex' id={el.value} classInput='radio__input input_hidden' classLabel='radio__label gender__label'
                                        onChange={settingsChange} labelValue={el.label} value={el.value.toLowerCase()} current={curSex} />
                                </li>
                            )}
                        </Submenu>
                    </li>
                    <li className="settings__item">
                        <Submenu name="age" additionalClass=''>
                            <DoubleRangeSlider />
                        </Submenu>
                    </li>
                    <li className="settings__item">
                        <Submenu name="sport" additionalClass='settings__submenu_with-chk'>
                            {sports.map((el) =>
                                <li className="submenu__item submenu_with-chk__item" key={el.value}>
                                    <Checkbox name='sports' current={curSports} onChange={settingsChange} label={el.label} value={el.label.toLowerCase()} />
                                </li>
                            )}
                        </Submenu>
                    </li>
                    <li className="settings__item">
                        <Submenu name="country" additionalClass='settings__submenu_with-chk'>
                            {countries.map((el) =>
                                <li className="submenu__item submenu_with-chk__item" key={el.value}>
                                    <Checkbox name='countries' current={curCountries} onChange={settingsChange} label={el.label} value={el.label.toLowerCase()} />
                                </li>
                            )}
                        </Submenu>
                    </li>
                </ul>
            </div>
        </>
    )
}

export default Filter;