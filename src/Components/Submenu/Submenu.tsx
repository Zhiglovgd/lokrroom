import './Submenu.css';
import React from 'react'
import {ReactComponent as ArrowIcon} from '../../assets/icons/arrow.svg'
import { useState } from 'react'

type Props = {
    name: string,
    children: React.ReactNode,
    additionalClass?: string,
}

export const Submenu: React.FC<Props> = ({ children, name, additionalClass }) => {
    const [open, setOpen] = useState(false)
    const handleClick = () => {
        setOpen((prev) => !prev)
    }

    return (
        <>
            <div className={`settings__menu-item ${open ? " " : "settings__menu-item_close"}`}>
                <span className="settings__name">
                    {name}
                </span>
                <div className="settings__arrow" onClick={() => handleClick()}>
                    <ArrowIcon className='arrow-icon'/>
                </div>
            </div>
            <ul className={"settings__submenu " + additionalClass} >
                <>
                    {children}
                </>
            </ul>
        </>
    )
}
