import './Checkbox.css';

type Props = {
className?: string,
onChange:  (e: React.ChangeEvent<HTMLInputElement>, type?:string) => void,
label: string,
value: string,
name: string,
current: Array<String> | [],
}

const Checkbox: React.FC<Props> = ({className = "", onChange, label, value, name, current }) => {
    return (
        <>
            <label className={"checkbox__label submenu_with-chk__label " + className }>
                <input type="checkbox" name={name} className="checkbox__input input_hidden" value={value} onChange={(e) => onChange(e)}
                // @ts-ignore: Unreachable code error 
                checked={current.includes(value)}/>
                <span>{label}</span>
            </label>
        </>
    )
}

export default Checkbox;