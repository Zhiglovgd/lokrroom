import { useState } from 'react';
import './Card.css';
import Avatar from '../../assets/images/avatar.png'

type Props = {
  user: {
    id: number,
    name: string,
    post: string,
    sport: string,
    contact: boolean,
  }
  onClick: (id: number) => void,

}

const Card: React.FC<Props> = ({ user, onClick,}) => {
  const [isFriend, setIsFriend] = useState(user.contact);

  const handleClick = () => {
    setIsFriend(prev => !prev);
    onClick(user.id);
  }

  return (
    <div className="card">
      <div className="card__image-container">
        <img src={Avatar} alt="User avatar" className="card__image" />
      </div>
      <span className="card__name">{user.name}</span>
      <span className="card__post">{user.post}</span>
      <span className="card__sport">{user.sport}</span>
      <button className="card__btn" onClick={handleClick}>{isFriend ? "message" : "+ connect"}</button>
    </div>
  )
}

export default Card;