import { useState } from 'react';
import { Sports, Gender, CountriesEn } from '../../selectsData';
import useLocalStorage from 'use-local-storage';
import {ReactComponent as FilterIcon} from '../../assets/icons/filter.svg'
import Filter from '../../Components/Filter/Filter';
import { Radio } from '../../Components/Radio/Radio';
import Card from '../../Components/Card/Card'
import { filterQuery, userData, Items } from "../../types"
import './App.css';

type Props = {
  usersData: Array<userData>
}
const defaultQuery: filterQuery = {
  filter: "all",
  sex: "all",
  sports: [],
  countries: [],
}

function searchFilter(text: string, data: Items) {
  let list: Items = [];
  if (text !== "") {
    list = data.filter((user) => {
      const names = user.name.toLowerCase().split(" ");
      let res = false;
      for (const name of names) {
        res = name.startsWith(text.toLowerCase())
        if (res) return res
      }
      return res;
    })
  } else { list = [...data] }
  return list
}

function filterByQuery(data: Items, settings: filterQuery) {
  return data.filter((el) => {
    if ((settings.sex === "all") ? true : settings.sex === el.sex) {
      // @ts-ignore: Unreachable code error
      if ((settings.sports.length === 0) ? true : settings.sports.includes(el.sport.toLowerCase())) {
        // @ts-ignore: Unreachable code error
        if ((settings.countries.length === 0) ? true : settings.countries.includes(el.country.toLowerCase())) {
          return true
        }
      }
    }
    return false
  }
  )
}

const App: React.FC<Props> = ({ usersData }) => {
  // @ts-ignore: Unreachable code error
  const [theme, setTheme] = useLocalStorage('theme' ? 'dark' : 'light');
  const [filterVisible, setFilterVisible] = useState(false);
  const [filterQuery, setFilterQuery] = useState(defaultQuery);
  const [users, setUsers] = useState(usersData);
  const [currentSearch, setCurrentSearch] = useState("");

  const handleOpenFilter = () => {
    setFilterVisible(prev => !prev);
  }

  const handleSwitchTheme = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTheme(e.target.value);
  }

  const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCurrentSearch(e.target.value)
  }

  const handleChangeContact = (id: number) => {
    const index: number = users.findIndex((el) => {
      return el.id === id;
    });
    setUsers((prev) => {
      return [...prev.slice(0, index),
      { ...prev[index], contact: !prev[index].contact },
      ...prev.slice(index + 1)
      ]
    });
  };

  const handleChangeQuery = (e: React.ChangeEvent<HTMLInputElement>) => {
    const category = e.target.name;
    if (category) {
      if (e.target.type === "radio") {
        if (category === 'filter' && filterQuery.filter === "draft") {
          setFilterQuery(defaultQuery);
        } else {
          setFilterQuery((prev) => {
            return { ...prev, ...{ [category]: e.target.value }, ...{ filter: "draft" } }
          })
        }
      }
      if (e.target.type === "checkbox") {
        if (category === "sports" || "countries" === category) {
          setFilterQuery((prev) => {
            const newData = { ...prev, ...{ filter: "draft" } }
            if (!e.target.checked) {
              // @ts-ignore: Unreachable code error
              const index = newData[category].indexOf(e.target.value);
              newData[category] = [...newData[category].slice(0, index), ...newData[category].slice(index + 1)];
              return newData;
            } else {
              // @ts-ignore: Unreachable code error
              newData[category] = [...newData[category], e.target.value]
              return newData;
            }
          })
        }
      }
    }
  }

  const visiblList = (filterQuery.filter === "draft") ? filterByQuery(searchFilter(currentSearch, users), filterQuery) : searchFilter(currentSearch, users);

  return (
    <div className='App' data-theme={theme}>
      <div className='main'>
        <header className="theme-container">
          <p className="theme__name">theme</p>
          <div className="theme__radio">
            <Radio name='theme' id='dark' classInput="radio__input" classLabel='radio__label theme__label'
              onChange={handleSwitchTheme} labelValue='Dark' value='dark' current={String(theme)} />
            <Radio name='theme' id='light' classInput="radio__input" classLabel='radio__label theme__label'
              onChange={handleSwitchTheme} labelValue='Light' value='light' current={String(theme)} />
          </div>
        </header>
        <div className="filter-mobile">
          <span className="filter-mobile__name">filter</span>
          <div className="filter-mobile__btn" onClick={handleOpenFilter}>
            {filterVisible ?
              <div className="filter-mobile__btn filter-mobile__btn_close">
              </div> :
              <FilterIcon className="filter-mobile__icon"/>
            }
          </div>

        </div>
        <aside className={`filter-container ${filterVisible?"filter-container_mobile-open":""}`}>
          <Filter curSports={filterQuery.sports} curCountries={filterQuery.countries} curSex={filterQuery.sex} curFilter={filterQuery.filter}
            settingsChange={handleChangeQuery} inputChange={handleChangeSearch} inputValue={currentSearch} sports={Sports} genders={Gender}
            countries={CountriesEn} />
        </aside>
        <main className="container">
          <ul className="list">
            {visiblList.map((el) =>
              <li className="list__item" key={el.id}>
                <Card user={el}  onClick={handleChangeContact}  />
              </li>
            )}
          </ul>
        </main>
      </div>
    </div>
  )
};

export default App;
