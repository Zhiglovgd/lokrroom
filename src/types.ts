export type setting = {
    label: string,
    value: string,
}

export type filterQuery = {
    filter: string,
    sex: string,
    sports: Array<string> | [],
    countries: Array<string> | [], 
}

export type country = {
    id: number,
    label: string, 
    alpha2: string,
    value: string,
}

export type userData = {
    id:number,
    name: string,
    sex: string,
    post: string,
    sport: string,
    country:string,
    age: number,
    contact: boolean, 
}

export type Items = Array<userData> | [];